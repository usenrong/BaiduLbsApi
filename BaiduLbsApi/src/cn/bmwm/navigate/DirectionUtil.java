
package cn.bmwm.navigate;

import com.meterware.httpunit.*;
import com.sun.org.apache.xpath.internal.XPathAPI;
//import org.jivesoftware.spark.util.log.Log;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.swing.DefaultListModel;

/**
 * A utility class that uses google's translation service to translate text to various languages.
 */
public class DirectionUtil {
    private static final String output = "xml";
    private static final String ak = "k6LRNg83n9cTeZIgqAClxL4Z";
    private static final DefaultListModel<String> resultListModel = new DefaultListModel();
    private DirectionUtil() {

    }



    public static DefaultListModel<String> navigate(String origin, String destination, String origin_region, String destination_region) {
    	
        String response = "";
        String urlString = "http://api.map.baidu.com/direction/v1?mode=driving&origin=" + origin +
                                        "&destination=" + destination +
                                        " &origin_region=" + origin_region +
                                        "&destination_region=" + destination_region +
                                        "&output=" + output +
                                         "&ak=" + ak;
        // disable scripting to avoid requiring js.jar
        HttpUnitOptions.setScriptingEnabled(false);

        // create the conversation object which will maintain state for us
        WebConversation wc = new WebConversation();

        // Obtain the baidu navigate result
        WebRequest webRequest = new GetMethodWebRequest(urlString);
        // required to prevent a 403 forbidden error from baidu
        webRequest.setHeaderField("User-agent", "Mozilla/4.0");
        try {
            WebResponse webResponse = wc.getResponse(webRequest);
            //NodeList list = webResponse.getDOM().getDocumentElement().getElementsByTagName("div");
            try {
				NodeList list2 = XPathAPI.selectNodeList(webResponse.getDOM(), "//steps/content/instructions/text()");
				for (int i = 0; i < list2.getLength(); ++i)
				{
                                       resultListModel.addElement("<html>"+list2.item(i).getNodeValue()+"</html>");
					//response = response + list2.item(i).getNodeValue()+" \n";
				}

            } catch (TransformerException e) {
//				Log.warning("Translator error",e);
			}
            
//            int length = list.getLength();
//            for (int i = 0; i < length; i++) {
//                Element element = (Element)list.item(i);
//                if ("result_box".equals(element.getAttribute("id"))) {
//                    Node translation = element.getFirstChild();
//                    if (translation != null) {
//                        response = translation.getNodeValue();
//                    }
//                }
//            }
        }
        catch (MalformedURLException e) {
//            Log.error("Could not for url: " + e);
        }
        catch (IOException e) {
//            Log.error("Could not get response: " + e);
        }
        catch (SAXException e) {
//            Log.error("Could not parse response content: " + e);
        }

      //  return response;
        return resultListModel;
    }

}

